package ru.tinkoff.load.myservice

import ru.tinkoff.gatling.config.SimulationConfig._
import io.gatling.core.Predef._
import ru.tinkoff.load.myservice.scenarios._
import ru.tinkoff.gatling.influxdb.Annotations
import ru.tinkoff.load.jdbc.Predef._

class MaxPerformance extends Simulation with Annotations {

  setUp(
    CommonScenario().inject(
      incrementUsersPerSec((intensity / stagesNumber).toInt) // интенсивность на ступень
        .times(stagesNumber) // Количество ступеней
        .eachLevelLasting(stageDuration) // Длительность полки
        .separatedByRampsLasting(rampDuration) // Длительность разгона
        .startingFrom(0) // Начало нагрузки с
    )
  ).protocols(jdbcProtocol)
    .maxDuration(testDuration) // общая длительность теста

}
